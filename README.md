# Desafio 2 - Agriness - Desenvolvimento

## Introdução

Bem-vindo ao Desafio Agriness. 

Esse desafio tem apenas o objetivo de entender como você aplicaria alguns conceitos técnicos que consideramos importantes durante o desenvolvimento de um software.

A seguir descrevemos três problemas que deverão ser resolvidos utilizando Python/SQL. Considere como requisitos não funcionais os seguintes pontos:

1. Utilização do estilo de código da linguagem Python (PEP8) e SQL;
2. Design da solução;

==========================================================================

## 1 - Inteiro Único

Você recebeu uma lista de inteiros. Todos os inteiros, exceto um, ocorrem duas vezes. Construa uma solução que dada uma lista seja exibido o inteiro único.

### Dados de Entrada

* `n` inteiros separados por espaço.

```
a1 a2 a3 a4 a5
```

### Premissas:

* 1 <= `n` <= 100
* É garantido que `n` é ímpar e que existe um inteiro único.
* 0 <= `ai` <= 100, onde 0 <= i < `n`.

### Formato da Saída

* Imprimir o ínteiro único.

### Exemplo

Dada a lista de inteiros:

```
1 1 2 3 3 4 5 4 5
```

A saída esperada é:

```
2
```

==========================================================================

## 2 - Flipping Bits

Você recebeu uma lista de 32 inteiros sem sinal (unsigned integers). Flip todos os bits (1->0 e 0->1) e imprima o resultado com um inteiro sem sinal.

### Dados de Entrada

* A primeira linha contém `T`, o número de casos de testes
* As linhas `T` linhas seguintes contém um inteiro a ser processado.

### Premissas

* 1 <= T <= 100

### Formato da Saída

* Imprima um linha por elemento da lista com o valor resultante.

### Exemplo

Dada a entrada:

```
3
2147483647
1
0
```

A saída esperada é:

```
2147483648
4294967294
4294967295
```

Explicação:

```
2147483647 = 01111111111111111111111111111111
             10000000000000000000000000000000 = 2147483648

         1 = 00000000000000000000000000000001 
             11111111111111111111111111111110 = 4294967294

         0 = 00000000000000000000000000000000 
             11111111111111111111111111111111 = 4294967295
```

==========================================================================

## 3 - SQL

Dada uma tabela com dados de estações meteorológicas realize as operações descritas abaixo:

DDL da tabela `STATION`:

``` 
ID: NUMBER - Id da estação
CITY: VARCHAR2(21) - Nome da cidade onde está localizada a estação.
STATE: VARCHAR2(2) - Código do Estado onde está localizada a estação.
TEMPERATURE: NUMBER - Última temperatuda capturada;
LAT_N: NUMBER - Latitude da localização
LONG_W: NUMBER - Longitude da localização.
``` 

Operações:

- Some todas as latitudes e longitudes (separadamente) arredondando o resultado em duas casas decimais.

    Exemplo de saída:

```    
lat lon

42850.04 47381.48
```

- Encontre todas as estações localizadas no Estado `SC` e cidade `Florianópolis ` cuja temperatura esteja abaixo de `10` graus;

- Encontre a menor temperatura registrada;

- Encontre a maior temperatura registrada.



==========================================================================

## Observações:

- Organize somente um reposiório com todas as soluções (separadas em arquivos diferentes, diretórios, etc - como achar melhor).
- Documente o projeto para que outros desenvolvedores possam executá-lo facilmente.
- Ao concluir compartilhe conosco o repositório GIT (Bitbucket ou GitHub). Certifique-se que teremos acesso total a ele. 
- A implementação de testes é um diferencial importante.
